using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MovimientoDelPersonaje : MonoBehaviour
{
    AudioClip clip;

    Animator Player;

    public int vida = 1;
    Rigidbody2D rb;
    float speed = 6f;


     void Start()
    {
        
    Player = GetComponent<Animator>();

        rb = GetComponent<Rigidbody2D>();

    }

    
    void Update()
    {
       
        
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        float verticalInput = Input.GetAxisRaw("Vertical");


        if (Input.anyKey)
        {
        
            

            Player.SetBool("isIdleUp", false);
            Player.SetBool("isIdleRight", false);
            Player.SetBool("isIdleLeft", false);

        }


        if (horizontalInput > 0)
        {
            verticalInput = 0;

            Player.SetBool("isRunningRight", true);

        


            if (Input.anyKeyDown ) {  
                
                Player.SetBool("isIdleRight", false);


            }
            else
            {
            Player.SetBool("isIdleRight", true);

            }



        }
        else
        {
            Player.SetBool("isRunningRight", false);
        }
        
        
        if(verticalInput > 0)
        {   
            horizontalInput = 0;

            Player.SetBool("isRunningUp", true);


            if (Input.anyKeyDown)
            {
                Player.SetBool("isIdleUp", false);
            }
            else
            {
                Player.SetBool("isIdleUp", true);

            }
        }
        else
        {
          
            Player.SetBool("isRunningUp", false);
           
        }

        if (horizontalInput < 0)
        {         
            verticalInput = 0;

            Player.SetBool("isRunningLeft", true);

            if (Input.anyKeyDown)
            {
                Player.SetBool("isIdleLeft", false);
            }
            else
            {
                Player.SetBool("isIdleLeft", true);

            }

        }
        else
        {

            Player.SetBool("isRunningLeft", false);
        }
        
        
        if (verticalInput < 0)
        {
            Player.SetBool("isRunning", true);

         

            horizontalInput = 0;
        }
        else {
            Player.SetBool("isRunning", false);
        }


        if (horizontalInput != 0)
        {
            transform.position += new Vector3(horizontalInput * speed * Time.deltaTime, 0, 0);

        }

        if (verticalInput != 0)
        {
            transform.position += new Vector3(0, verticalInput * speed * Time.deltaTime, 0);

        }





        Vector2 moveDirection = new Vector2(horizontalInput, verticalInput);


       moveDirection.Normalize();


       
        if (Input.anyKey)
        {
            rb.velocity = moveDirection * speed;
        }
        else
        {
            rb.velocity = Vector2.zero;
                
         }

        

        


       


        



    }

    void OnTriggerEnter2D(Collider2D other) {


        if (other.CompareTag("SiguienteNivel"))
        {
            SceneManager.LoadScene("Nivel Inicial");


            Debug.Log("SI");
          

        }
        vida = vida - 1;
        
    
    }

    //void RestrictMovement()
    //{

    //    float clampedX = Mathf.Clamp(transform.position.x, -10f, 10f);
    //    float clampedY = Mathf.Clamp(transform.position.y, -5f, 5f);

    //    if(Mathf.Abs(transform.position.x - clampedX) < Mathf.Epsilon)
    //    {
    //        clampedX = transform.position.x;
    //    }

    //    if (Mathf.Abs(transform.position.y - clampedY) < Mathf.Epsilon)
    //    {
    //        clampedY = transform.position.y;
    //    }

    //    transform.position = new Vector2(clampedX, clampedY);

    //}



}
