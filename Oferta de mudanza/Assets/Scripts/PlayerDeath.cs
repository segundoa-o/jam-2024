using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerDeath : MonoBehaviour
{
    private Animator animator; // Referencia al componente Animator
    private bool isDead = false; // Variable para controlar si el personaje ha muerto

    private void Awake()
    {
 //       animator = GetComponent<Animator>(); // Obtener la referencia al componente Animator
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Trampa") && !isDead)
        {


            // Desactivar el movimiento del jugador
            GetComponent<MovimientoDelPersonaje>().enabled = false;

            // Activar el trigger de muerte
   //         animator.SetTrigger("isDead");

            // Reiniciar nivel después de un tiempo
            Invoke(nameof(RestartLevel), 0.1f);
        }
    }

    private void RestartLevel()
    {
        
        SceneManager.LoadScene(4);
    }
}
